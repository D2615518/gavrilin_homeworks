
package com.innopolice.Attestation01;

/* Реализовать в классе UsersRepositoryFileImpl методы:

        User findById(int id);
        update(User user);

        Принцип работы методов:

        Пусть в файле есть запись:

        1|Игорь|33|true

        Где первое значение - гарантированно уникальный ID пользователя (целое число).

        Тогда findById(1) вернет объект user с данными указанной строки.

        Далее, для этого объекта можно выполнить следующий код:

        user.setName("Марсель");
        user.setAge(27);

        и выполнить update(user);

        При этом в файле строка будет заменена на 1|Марсель|27|true.

        Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.

        Примечания:
        Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)

 */


public class Attestation01 {

    public static void main(String[] args) {

        UsersRepository repository = new UsersRepositoryFileImpl("user.txt");
        User user = repository.findById(4);
        user.setName("Марсель");
        user.setAge(29);
        repository.update(user);
    }
}
