
package com.innopolice.homework21;

public class SumThread extends Thread {

    private int from;
    private int to;
    private int index;

    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
        index = Homework21.countThreads;
    }
    int sum;
    @Override
    public void run() {

        for (int i = from; i < (to + 1); i++) {
            sum += Homework21.array[i];
        }
        Homework21.sums[index] = sum;
    }
}
