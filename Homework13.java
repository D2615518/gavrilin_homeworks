package com.innopolice.homework13;

import java.util.Arrays;

/*
Предусмотреть функциональный интерфейс

interface ByCondition {
	boolean isOk(int number);
}

Реализовать в классе Sequence метод:

public static int[] filter(int[] array, ByCondition condition) {
	...
}

Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.

В main в качестве condition подставить:

- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.

*/

public class Main {

    public static void main (String[] args) {

        int[] array = {245, 112, 225, -111, -205, 1458, 500, 11, 33, 100};

        // -> Синтаксисис Lambda выражения (аргумент1, аргумент2) -> { тело } //

        int[] filter1 = Sequence.filter (array, digit -> digit % 2 == 0);

        ByCondition byCondition = digit -> {

            int sum = 0;

            while (digit != 0) {
                int recent = digit % 10;
                sum = sum + recent;
                digit = digit / 10;
            }

                return sum % 2 == 0;
        };

        int[] filter2 = Sequence.filter (array, byCondition);

        System.out.println ("parity check - " + Arrays.toString(filter1));
        System.out.println ("checking if the sum of digits of an element is an even number - " + Arrays.toString(filter2));
    }
}





