package com.innopolice.homeworks17;

/* С помощью HashMap collection вывести сколько каждое слово
   встречается в тексте.
*/


import java.util.*;

public class Main {
    public static void main(String[] args)
    {
        System.out.println("Введите текст");

        // считываем значения с консоли //
        Scanner in = new Scanner(System.in);
        String string = in.nextLine();

        /*
        Метод split возвращает массив String (слов в данном случае),
        поэтому нет необходимости вручную добавлять слова в массив.
        Также split сделан по "одному или нескольким пробельным символам"
        с помощью регулярного выражения, что помогает избежать проблем с
        пустыми словами из-за идущих подряд пробелов.
        */

        String[] words = string.split("\\s+");

        // реализация с помощью HashMap
        HashMap<String, Integer> wordToCount = new HashMap<>();
        for (String word : words)
        {
            if (!wordToCount.containsKey(word))
            {
                wordToCount.put(word, 0);
            }
            wordToCount.put(word, wordToCount.get(word) + 1);
        }
        for (String word : wordToCount.keySet())
        {
            System.out.println(word + " " + wordToCount.get(word));
        }
    }

}
