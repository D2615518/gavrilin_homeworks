package com.innopolice.homeWork19;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

        System.out.println("Найдены все пользователи");
        output(usersRepository.findAll());

        User userIgor = new User("Игорь", 33, true);
        if (!users.contains(userIgor))
            usersRepository.save(userIgor);

        System.out.println("Поиск по возрасту");
        output(usersRepository.findByAge(22));

        System.out.println("findByIsWorkerIsTrue ");
        output(usersRepository.findByIsWorkerIsTrue());
    }

    public static void output(List<User> users) {
        for (User user : users)
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
    }
}
