package com.innopolice.homework16;

public class LinkedList<T> {

    private static class Node<T> {
        T value;
        com.innopolice.homework16.LinkedList.Node<T> next;
        com.innopolice.homework16.LinkedList.Node<T> prev;

        public Node(T value) {
            this.value = value;
        }
    }

    private com.innopolice.homework16.LinkedList.Node<T> first;
    private com.innopolice.homework16.LinkedList.Node<T> last;
    private int size;

    private com.innopolice.homework16.LinkedList.Node<T> node(int index) {
        com.innopolice.homework16.LinkedList.Node<T> node;
        if (index < size / 2) {
            node = first;
            for (int i = 0; i < index; i++)
                node = node.next;
        } else {
            node = last;
            for (int i = size - 1; i > index; i--)
                node = node.prev;
        }
        return node;
    }

    private void searchValue(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException(String.format("Index %s out of bounds for length %s", index, size));
    }

    public void add(T element) {
        // создаю новый узел
        com.innopolice.homework16.LinkedList.Node<T> newNode = new com.innopolice.homework16.LinkedList.Node<>(element);
        com.innopolice.homework16.LinkedList.Node<T> prevNode = last;
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        last.prev = prevNode;
        size++;
    }

    public void addToBegin(T element) {
        com.innopolice.homework16.LinkedList.Node<T> newNode = new com.innopolice.homework16.LinkedList.Node<>(element);
        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
            first.prev = newNode;
        }
        first = newNode;
        size++;
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        searchValue(index);
        return node(index).value;
    }

    public void remove(int index) {
        searchValue(index);

        com.innopolice.homework16.LinkedList.Node<T> prevNode = node(index).prev;
        com.innopolice.homework16.LinkedList.Node<T> nextNode = node(index).next;

        if (prevNode == null)
            first = nextNode;
        else
            prevNode.next = nextNode;

        if (nextNode == null)
            last = prevNode;
        else
            nextNode.prev = prevNode;

        size--;
    }

    @Override
    public String toString() {
        com.innopolice.homework16.LinkedList.Node<T> node = first;
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < size; i++) {
            buffer.append(node.value);
            node = node.next;
            if (i < size - 1) buffer.append(",");
        }

        return buffer.toString();
    }
}

