
package ru.pcs.com.foodservice.repos;

import ru.pcs.com.foodservice.entities.Order;
import ru.pcs.com.foodservice.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepo extends CrudRepository<Order, Integer> {
    List<Order> findAllByUser(User user);
}