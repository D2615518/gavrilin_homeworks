import java.util.Scanner;


public class My {
    public static void main(String[] args) {
        int minimum;
        int number;
        int digit;

        minimum = 10;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();// используем Java метод next() для считывания значений вводимых в консоль//

        while (number != -1) {              // цикл будет выполняться непрерывно, выход из цикла произойдет после ввода значения -1 в консоль.
            while (number != 0) {           // Вложенный цикл while (находящийся внутри "непрерывно выполняемого цикла" ) берет остаток от
                                            //деления числа вводимого в консоль. Далее отрабатывает условие, в котором происходит сравнее
                digit = number % 10;        // переменной digit и minimum, в случаи когда условие истинно переменной minimum присваивается
                                            // значение переменной digit.
                if (digit < minimum) {
                    minimum = digit;
                }
                else
                    number = number / 10;
            }
            number = scanner.nextInt();

        }
        System.out.println(minimum); // вывод в консоль  //


    }
}
