/*
Создать schema.sql файл, который содержит описание таблиц и данных для этих таблиц

Товар
- id
- описание
- стоимость
- количество

Заказчик
- id
- имя/фамилия

Заказ
- id-товара (внешний ключ)
- id-заказчика (внешний ключ)
- дата заказа
- количество товаров

Написать 3-4 запроса на эти таблицы.

 */

create table apartments
(
        id serial primary key,
        description varchar(40),
        cost double precision check ( quantity >0 ),
        quantity integer check ( quantity > 0 )
);

        insert into apartments(description, cost, quantity)
        VALUES ('2 комнатная кватрира площадью 74 кв.м', 306.50,3);
        insert into apartments(description, cost, quantity)
        VALUES ('1 комнатная кватрира площадью 52 кв.м', 75.00,2);
        insert into apartments(description, cost, quantity)
        VALUES ('Частный дом площадью 152 кв.м', 105.90,4);
        insert into apartments(description, cost, quantity)
        VALUES ('1 комнатная кватрира площадью 42 кв.м', 61.90,4);
        insert into apartments(description, cost, quantity)
        VALUES ('3 комнатная кватрира площадью 86 кв.м', 99.90,1);
        insert into apartments(description, cost, quantity)
        VALUES ('кватрира-студия площадью 31 кв.м', 80.99,5);


        create table customer (
        id serial primary key,
        first_name varchar(30)
        );

        insert into customer(first_name)
        VALUES ('Новиков');
        insert into customer(first_name)
        VALUES ('Иванов');
        insert into customer(first_name)
        VALUES ('Мухаметшин');
        insert into customer(first_name)
        VALUES ('Шулаев');
        insert into customer(first_name)
        VALUES ('Тургенев');

        create table orders(
        product_id integer,
        foreign key(product_id) references apartments(id),
        customer_id integer,
        foreign key (customer_id) references customer(id),
        date date,
        numberApartmens integer
        );

        insert into orders(product_id, customer_id, date, numberApartmens)
        VALUES (1,2,'2021-11-27',1);
        insert into orders(product_id, customer_id, date, numberApartmens)
        VALUES (2,2,'2021-11-27',1);
        insert into orders(product_id, customer_id, date, numberApartmens)
        VALUES (4,2,'2021-11-27',1);
        insert into orders(product_id, customer_id, date, numberApartmens)
        VALUES (3,1,'2021-11-27',1);
        insert into orders(product_id, customer_id, date, numberApartmens)
        VALUES (5,3,'2021-11-27',1);
        insert into orders(product_id, customer_id, date, numberApartmens)
        VALUES (2,4,'2021-11-27',3);


        select description from apartments where id = (select product_id from orders where customer_id = (select id from customer where first_name = 'Шулаев'));

        select first_name,(select count(*) from orders where customer_id = customer.id) as productCount from customer;

        select * from customer a full join orders o on a.id = o.customer_id;
        select *from apartments a full outer join orders o on a.id = o.product_id;