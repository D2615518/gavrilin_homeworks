
package com.innopolice.Attestation01;

// Создан интерфейс //
import java.util.List;

public interface UserRepository {
    List<User> findAll();
    User findById(int id);
    void update(User user);
}