package com.innopolice.homework16;

/*Реализовать removeAt(int index) для ArrayList
  Реализовать метод T get(int index) для LinkedList
*/
public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> digits = new ArrayList<>();
        digits.add(22);
        digits.add(38);
        digits.add(44);
        digits.add(8);
        digits.add(77);
        digits.add(210);
        digits.add(12);
        digits.add(100);
        digits.add(325);
        digits.add(177);
        digits.add(2);
        digits.add(5);

// Находим элемент с индексом 9 ( это цифра 177) и удалем //
        System.out.println("До удаления" + digits);
        digits.removeAt(9);
        System.out.println("После удаления" + digits);
    }

}