
package com.innopolice.Attestation01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;
    private List<User> users;


    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
        users = findAll();
    }

    @Override
    public List<User> findAll() {
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            return reader.lines()
                    .map(line -> {String[] strings = line.split("\\|");
                        return new User(Integer.parseInt(strings[0]), strings[1], Integer.parseInt(strings[2]), Boolean.parseBoolean(strings[3]));
                    })
                    .collect(Collectors.toCollection(ArrayList<User>::new));
        } catch (IOException e){
            throw new IllegalArgumentException();
        }
    }


    private void save() {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))){
            for (User user: users){
                writer.write(user.getId()+"|" + user.getName() +"|" + user.getAge() + "|" + user.isWorker());
                writer.newLine();
            }
        } catch (IOException e){
            throw new IllegalArgumentException();
        }
    }

    @Override
    public User findById(int id) {
        for (User us : users) {
            if (us.getId() == id) {
                return us;
            }
        }
        System.out.println("Пользователя не существует");
        throw new IllegalArgumentException();
    }

    @Override
    public void update(User user) {
        if(user==null)
            return;
        for(User us : users){
            if(us.getId() == user.getId()){
                us = user;
            }
        }
        save();
    }
}
