
package homework09;

//Создан класс Rectangle потомок класса Figure//

public class Rectangle extends Figure {

    public int sideOfTheRectangleA;
    public int sideOfTheRectangleB;


    public Rectangle (int x, int y, int sideOfTheRectangleA, int sideOfTheRectangleB) {
        super(x, y);

        this.sideOfTheRectangleA = sideOfTheRectangleA;
        this.sideOfTheRectangleB = sideOfTheRectangleB;
    }

    public int getPerimeter() {
        System.out.println("Rectangle area is");
        return 2 * (sideOfTheRectangleA + sideOfTheRectangleB);

    }
}
