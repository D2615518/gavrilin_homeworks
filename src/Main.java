package homework09;

public class Main {

    // Точка входа в программу //
    public static void main (String[] args) {

        // Вызов //
        Figure rectangle = new Rectangle(8,8, 10,10);
        Figure circle = new Circle(5,5,10,10);
        Figure ellipse = new Ellipse(25, 25, 50, 60);
        Figure square = new Square(10, 10, 5, 5);

        // Вывод на экран c обращение к методу getPerimeter //
        System.out.println(rectangle.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(ellipse.getPerimeter());
        System.out.println(square.getPerimeter());
    }
}
