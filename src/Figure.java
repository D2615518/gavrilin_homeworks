
package homework09;

// Создан класс Figure с кординатами 'X' и 'Y'//
public class Figure {
    private int x;
    private  int y;

    public Figure (int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Метод getPerimeter() который возвращает ноль //
    public int getPerimeter() {
        return 0;
    }

}
