
package homework09;

// Создан класс Ellipse потомок класса Figure//

public class Ellipse extends Figure {

    private final int radiusX;
    private final int radiusY;

    public Ellipse(int x, int y, int radiusX, int radiusY) {
        super(x, y);
        this.radiusX = radiusX;
        this.radiusY = radiusY;
    }

    public int getPerimeter() {
        System.out.println("ellipse area is");
        return (int) (4 * (Math.PI * radiusX * radiusY + (radiusX - radiusY)) / (radiusX + radiusY));

    }
}
