package com.innopolice.homework13;

// Создан функциональный интерфейс //
public interface ByCondition {

    boolean isOk(int number);
}