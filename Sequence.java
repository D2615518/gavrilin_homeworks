package com.innopolice.homework13;

import java.util.Arrays;

// Создан класс Sequence c реализацией метода по заданию //

public class Sequence {

    public static int[] filter (int[] array, ByCondition condition) {
        int[] result = new int[array.length];
        int count = 0;
        for (int c : array) {
            if (condition.isOk(c)) {
                result[count++] = c;
            }
        }

        // Использован метод copyOfRange, который копирует указанный диапазон массива в новый массив //
        return Arrays.copyOfRange(result, 0, count);
    }
}
