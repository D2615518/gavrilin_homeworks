
package ru.pcs.com.foodservice.repos;

import ru.pcs.com.foodservice.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepo extends CrudRepository<Product, Integer> {
    Product findFirstByName(String name);
}