
package ru.pcs;

public class NumbersUtil {

    public boolean isPrime(int number) {

        if (number == 0 || number == 1) {
            throw new IllegalArgumentException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int sum(int a, int b) {
        return a + b;
    }

    public int gcd(int a, int b) {
        if (a < 0 || b < 0){
            throw new IllegalArgumentException();
        }
        if(b == 0){
            return a;
        }
        return  gcd(b, a % b);
    }
}

