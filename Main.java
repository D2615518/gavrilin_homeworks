package homework08;

import java.util.Scanner;

public class Main {

/*
За основу взят алгоритм быстрой сортировки.
Выбран опорный элемент из массива.Разделен массив на два подмассива: элементы, меньше опорного и элементы,
больше опорного.
 */

    public static Person[] personArray(int length) {
        Person[] persons = new Person[length];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < persons.length; i++) {
            persons[i] = new Person();
            persons[i].setName(scanner.nextLine());
            persons[i].setWeight(Double.parseDouble(scanner.nextLine()));
        }
        scanner.close();
        return persons;
    }

    public static void quickSort(Person[] persons, int low, int high) {
        if (persons.length == 0) {
            return;
            //завершить выполнение если длина массива равна 0//
        }
        if (low >= high) {
            return;
            //завершить выполнение если уже нечего делить
        }

        // выбрать опорный элемент
        int pivot = (low + high) / 2;
        Person pivotPerson = persons[pivot];

        // разделить на подмассивы, который больше и меньше опорного элемента
        int i = low;
        int j = high;
        while (i <= j) {
            while (persons[i].getWeight() < pivotPerson.getWeight()) {
                i++;
            }
            while (persons[j].getWeight() > pivotPerson.getWeight()) {
                j--;
            }
            //меняем местами
            if (i <= j) {
                Person temp = persons[i];
                persons[i] = persons[j];
                persons[j] = temp;
                i++;
                j--;
            }
        }
        // вызов рекурсии для сортировки левой и правой части
        if (low < j) {
            quickSort(persons, low, j);
        }
        if (high > i) {
            quickSort(persons, i, high);
        }
    }

    public static void sortingWeight(Person[] persons  ) {
        for (int i = 0; i < persons.length; i++) {
            for (int j = i + 1; j < persons.length; j++) {
                if (persons[i].getWeight() > persons[j].getWeight()) {
                    Person temp = persons[i];
                    persons[i] = persons[j];
                    persons[j] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        Person[] persons = personArray(10);
        quickSort(persons, 0, persons.length - 1);

        for (Person person : persons) {
            System.out.println(person);
        }
    }
}

