
package com.innopolice.homeWork06;

import java.util.Arrays;

/**
 * Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
 *
 * Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
 *
 * было:
 * 34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
 *
 * стало
 * 34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
 */

public class Main {




    // создана функция возвращающая индекс числа в массиве //
    static int getIndex(int[] array, int val) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == val)
                return i;
        }

        return -1;
    }

    // создана процедура //
    public static void replaceLeft(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                for (int a = i;a < array.length; a++) {
                    if (array[a] != 0) {
                        array[i] = array[a];
                        array[a] = 0;
                        break;
                    }
                }

            }
        }
    }





    // точка входа в программу (main) с вызовом функции getIndex и вызовом процедуры replaceLeft//
    public static void main(String[] args) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7};

        System.out.println(getIndex(array, 3));
        System.out.println(getIndex(array, 4));
        System.out.println(getIndex(array, 5));

        int[] shortArray = {54, 0, 0, 31, 25, 0, 78, 0, 0, 4, 28};

        replaceLeft(shortArray);

        System.out.println(Arrays.toString(shortArray));

    }
}
