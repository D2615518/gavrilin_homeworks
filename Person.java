package homework08;

public class Person {

    private String name;
    private double weight;

    public String getName() {
        return name;
    }



    //  this ключевое слово используется когда у переменной экземпляра класса и переменной метода/конструктора одинаковые имена;
    //   Когда нужно вызвать конструктор одного типа (например, конструктор по умолчанию или параметризированный)
    //   из другого. Это еще называется явным вызовом конструктора.

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}

