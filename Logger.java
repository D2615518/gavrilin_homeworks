package com.innopolice.homeWork11;

//final - таким образом обьявляем константу в Java//
public class Logger {
    private static final Logger instance;

    /* Статический конструктор ( статический инициализатор) - создает единственный
       обьект ( экземпляр класса).
     */
    static {
        instance = new Logger();
    }


    // Метод void log (String massage) по заданию //
    public static Logger getInstance() {
        return instance;
    }

    public void log (String message) {

        System.out.println (message);
    }
}
