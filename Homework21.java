
package com.innopolice.homework21;

/*

Реализовать многопоточное суммирование элементов

*/

import java.util.Random;
import java.util.Scanner;

public class Homework21 {

    public static int array[];

    public static int sums[];

    public static int countThreads = 0;

    public static int sum;
    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];


        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }


        System.out.println (sum );


        int x = 0;
        int y =  numbersCount/(threadsCount-1);
        int over =  numbersCount%(threadsCount-1);
        int c = y - 1;

        SumThread[] sumThreads = new SumThread[threadsCount];

        while (countThreads  < threadsCount) {
            SumThread sumThread;
            if (countThreads == (threadsCount-1)) {
                sumThread = new SumThread(x, (x+(over-1)));
            }
            else {
                sumThread = new SumThread(x,c);
                x += y;
                c +=y;
            }
            sumThreads[countThreads] = sumThread;
            sumThread.start();
            countThreads++;
        }


        for(SumThread st: sumThreads) {


            // join метод, который может быть использован для того,
            // чтобы приостановить выполнение текущего потока до тех пор,
            // пока другой поток не закончит свое выполнение
            try {
                st.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException();
            }
        }

        int threadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            threadSum += sums[i];
        }
        System.out.println(threadSum + " Сумма ");
    }

}
